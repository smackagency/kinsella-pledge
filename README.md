# The Ben Kinsella Pledge App

Cordova 3.5.1 Android app.

#### Prerequisites
* npm

#### Getting Started
From the project root directory, run:

```
npm install -g plugman
npm install -g cordova
cordova platform add browser
cordova run browser
```