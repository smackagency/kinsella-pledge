(function() { 
    var app = angular.module('kinsella', ['onsen.directives', 'ngTouch','autocomplete']);
    var schoolName = 'None';
    app.factory('School',function(){
        return {
            getProperty: function(){
                return schoolName;
            },  
             setProperty: function(value) {
                schoolName = value;
            }
       }

    });

    app.directive('onLongPress',['$timeout','$swipe', function ($timeout, $swipe) {
      return {
          restrict: 'A',
          link: function ($scope, $elm, $attrs) {
 
              $swipe.bind($elm,{
                  'start': function (coords) {
                      // Locally scoped variable that will keep track of the long press
                      $scope.longPress = true;
 
                      // We'll set a timeout for 600 ms for a long press
                      $timeout(function () {
                          if ($scope.longPress) {
                              // If the touchend event hasn't fired,
                              // apply the function given in on the element's on-long-press attribute
                              $scope.$apply(function () {
                                  $scope.$eval($attrs.onLongPress)
                              });
                          }
                      }, 1000);
                  },
                  'move': function (coords) { 

                    $scope.longPress = true;
                  },
                  'end': function (coords) {
                      // Prevent the onLongPress event from firing
                      $scope.longPress = true;
                      // If there is an on-touch-end function attached to this element, apply it
                      if ($attrs.onTouchEnd) {
                          $scope.$apply(function () {
                              $scope.$eval($attrs.onTouchEnd)
                          });
                      }
                  },
                  'cancel': function (coords) { 

                    $scope.longPress = true;
                  }
              });
 
          }
      };
  }]);

    app.factory('Suffix',function(){
      return { 
        getOrdinal: function(i){
          var j = i % 10,
              k = i % 100;
          if (j == 1 && k != 11) {
              return i + "st";
          }
          if (j == 2 && k != 12) {
              return i + "nd";
          }
          if (j == 3 && k != 13) {
              return i + "rd";
          }
          return i + "th";
          }
      }
    });

    app.factory('SchoolRetriever', function($http,$rootScope, $q, $timeout){
      var SchoolRetriever = new Object();

      SchoolRetriever.getschools = function(i) {
        var schooldata = $q.defer();
        var schools;

      //$rootScope.remoteUrl='https://local.smackagency.com/kinsella-pledge-server/';
       $rootScope.remoteUrl='https://kinsella-pledge.smack.build/';
        $http.get($rootScope.remoteUrl + 'site/names').success(function(data){
           schools =  $rootScope.names = data;
         });

        $timeout(function(){
          schooldata.resolve(schools);
        },1000);

        return schooldata.promise
      }

      return SchoolRetriever;
});

 

  app.controller('Tester',function($scope,School,$rootScope){

      //$rootScope.remoteUrl='https://local.smackagency.com/kinsella-pledge-server/';
      $rootScope.remoteUrl='https://smackagency.com/kinsella-pledge/';

   
     function getQueryStringValue (key) {  
        return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
      }  

      var query = getQueryStringValue("school");
      // Would write the value of the QueryString-variable called name to the console  
      if(query){
        School.setProperty(query);
        $scope.ons.navigator.pushPage('ready.html', {animation: "fade"});
      }
      else {
          $scope.ons.navigator.pushPage('start.html', {animation: "fade"});
      }

  });
    app.controller('StartCtrl', function($scope,$rootScope,$http,School,limitToFilter,SchoolRetriever) {

      // Would write the value of the QueryString-variable called name to the console  
  
        document.addEventListener('deviceready', function() {
          document.addEventListener("showkeyboard", function(){
            document.getElementById('main-area').className += ' keyon';
          }, false);
       document.addEventListener("hidekeyboard", function(){ document.getElementById("main-area").className = "schoolPage";}, false)
             }, false);



         $scope.schools = SchoolRetriever.getschools("...");
          $scope.schools.then(function(data){
            $scope.schools = data;
          });

          $scope.getschools = function(){
            return $scope.schools;

            }
        
        $scope.next = function(index,school) {
            School.setProperty(school);

            if(school){
                var request = $http({
                    method: "post",
                    url: $rootScope.remoteUrl + 'site/school?name=' + school
                });

                // Store the data-dump of the FORM scope.
                request.success(
                    function( html ) {
                        $scope.ons.navigator.pushPage('ready.html', {animation: "fade"});
                    }
                );
            }            
        };
    });


    //create controller
    app.controller('ReadyCtrl', function($scope,$rootScope,School,$http) {
        window.plugins.html5Video.initialize({ 'kinsella': 'ben.mp4' });
        
        $scope.school = School.getProperty();
        $scope.next = function(index,school) {
            var starting = $http({
                    method: "post",
                    url: $rootScope.remoteUrl + '/site/begin?name=' + $scope.school
                });

                // Store the data-dump of the FORM scope.
                starting.success(
                    function( html ) {
 
                        $scope.ons.navigator.pushPage('takepledge.html', {animation: "fade"});
 
                    }
                );
        }

    });

    app.controller('SeeCtrl', function($scope,$rootScope,School) {
        $scope.next = function(index,school) {
             $scope.ons.navigator.pushPage('takepledge.html', {animation: "fade"});
        }
    });

     app.controller('TakeCtrl', function($scope,$rootScope,School) {
        $scope.itemOnLongPress = function() {
           $scope.ons.navigator.resetToPage('final.html', {animation: "fade"});
        }
         
        $scope.next = function(index,school) {
             $scope.ons.navigator.pushPage('seepledge.html', {animation: "fade"});
        }
    });


   app.controller('FinalCtrl', function($scope,$rootScope,School,$http,Suffix) {
         document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);

         $scope.classname = 'inactive';

         setTimeout(function() {
             window.plugins.html5Video.play('kinsella');
             $scope.classname = 'active';
             document.getElementById('kinsella').classList.add('active');
             document.getElementById('thefade').classList.add('active');
             document.getElementById('ct').classList.add('active');
         }, 1000);

          $scope.school = School.getProperty();
          $http.get($rootScope.remoteUrl + 'site/pledges').success(function(data){
            $scope.count=Suffix.getOrdinal(data);
         });
    
          $scope.next = function(index,school) {
             // $scope.ons.navigator.pushPage('ready.html', {animation: "fade"});
         navigator.app.loadUrl('file:///android_asset/www/index.html?school='+$scope.school);
        }

    });

})();
